import { Database } from 'bun:sqlite'
import { Elysia } from 'elysia'
import { rateLimit } from 'elysia-rate-limit'
import { cron } from '@elysiajs/cron'

const db = new Database('door.sqlite')
const elysia = new Elysia()

const tempCodes = new Map()

elysia.use(cron({
    name: 'doorTempCodeCleaner',
    pattern: '*/14 * * * * *',
    run() {
        if(tempCodes.size) {
            tempCodes.forEach((value, key) => {
                if(value.time < Math.round(Date.now()/1000)-14) {
                    tempCodes.delete(key)
                }
            })
        }
    }
}))

elysia.use(rateLimit({
    duration: 7000,
    max: 3,
    responseMessage: 'Rate-limit reached'
}))

elysia.get('/:code', ({ params, request, set }) => {
    // if(!request.headers.get('user-agent').startsWith('Unity')) { set.status = 403; return 'Forbidden' }
    set.status = 406
    if(params.code.length !== 3) return 'Wrong code length'
    if(isNaN(params.code)) return 'Code is NaN'
    if(params.code.includes('-') || params.code.includes('.')) return 'Invalid code'

    set.status = 200
    const ip = request.headers.get('cf-connecting-ip') || request.headers.get('x-forwarded-for')
    if(!tempCodes.get(ip)) {
        tempCodes.set(ip, {
            time: Math.round(Date.now()/1000),
            code: params.code
        })
        return params.code+'...'
    } else {
        const fullCode = tempCodes.get(ip).code + params.code
        tempCodes.delete(ip)
        const destination = getDestinationByCode(fullCode)
        if(!destination) { set.status = 404; return 'Door not found' }
        return destination
    }
})

elysia.get('/', () => Bun.file('./index.html'))

elysia.post('/create', ({ body, request, set }) => {//Create Door
    set.status = 200//actually 406 htmx pls fix
    if(isNaN(body.code)) return '<div class="err">Code is NaN</div>'
    if(body.code.includes('-') || body.code.includes('.')) return '<div class="err">Invalid code</div>'
    if(body.code < 100 || body.code.length > 6) return '<div class="err">Code is too short</div>'
    if(body.password.length > 20) return '<div class="err">Password is too long</div>'
    const destPtrn = ['https://vrchat.com/home/world/', 'https://vrchat.com/home/launch?worldId=']
    if(!(body.destination.startsWith(destPtrn[0]+'wrld_') || body.destination.startsWith(destPtrn[1]+'wrld_'))) return '<div class="err">Invalid world/instance link</div>'
    const ip = request.headers.get('cf-connecting-ip') || request.headers.get('x-forwarded-for')
    // if(isUserInCooldown(ip, 3600)) return '<div class="err">You can make doors only once in a hour</div>'

    const parsedDestination = body.destination.replace(destPtrn[0], '').replace(destPtrn[1], '').replace('&instanceId=', ':')
    const door = {
        code: body.code,
        updatedAt: Math.round(Date.now()/1000),
        lastIP: ip,
        password: body.password,
        destination: parsedDestination
    }
    if(!createDoor(door)) {
        // if(isUserInCooldown(ip, 60)) return '<div class="err">You can only update doors once per minute</div>'
        if(!updateDoor(door)) return '<div class="err">Door with this code already exists or password for update is incorrect</div>'
        return '<div class="ok">Door succesfully updated!</div>'
    }
    set.status = 200
    return '<div class="ok">Door succesfully created!</div>'
})

elysia.listen(3223, () => console.log('Elysia started on port ' + elysia.server.port))

function createDoor({ code, updatedAt, lastIP, password, destination }) {
    if(getDestinationByCode(code)) return false
    const sql = db.query('INSERT INTO door VALUES ($code, $updatedAt, $lastIP, $password, $destination)')
    sql.run({
        $code: parseInt(code),
        $updatedAt: updatedAt,
        $lastIP: lastIP,
        $password: password,
        $destination: destination
    })
    console.log('NEW DOOR: ' + code + ' ' + destination)
    return true
}

function updateDoor({ code, updatedAt, lastIP, password, destination }) {
    const sql = db.query('SELECT password FROM door WHERE code = $code LIMIT 1')
    if(sql.get({ $code: code }).password !== password) return false
    const sql2 = db.query('UPDATE door SET updatedAt=$updatedAt, lastIP=$lastIP, destination=$destination WHERE code=$code')
    sql2.run({
        $code: parseInt(code),
        $updatedAt: updatedAt,
        $lastIP: lastIP,
        $destination: destination
    })
    console.log('UPDATED DOOR: ' + code + ' ' + destination)
    return true
}

function getDestinationByCode(code) {
    const sql = db.query('SELECT destination FROM door WHERE code = $code LIMIT 1')
    const response = sql.get({ $code: parseInt(code) })
    if(!response) return false
    return response.destination
}

function isUserInCooldown(ip, cooldown) {
    const sql = db.query('SELECT updatedAt FROM door WHERE lastIP = $lastIP ORDER BY updatedAt DESC LIMIT 1')
    const response = sql.get({ $lastIP: ip })
    if(!response) return false
    if(response.updatedAt+cooldown > Math.round(Date.now()/1000)) return true
    return false
}