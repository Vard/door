import { Database } from "bun:sqlite";

const db = new Database("door.sqlite", { create: true });

const query = db.query('CREATE TABLE "door" (  "code" INTEGER NOT NULL CHECK(length("code") < 7) UNIQUE, "updatedAt" INTEGER NOT NULL CHECK(length("updatedAt") > 9),  "lastIP" TEXT NOT NULL CHECK(length("lastIP") > 7),  "password" TEXT NOT NULL,  "destination" TEXT NOT NULL,  PRIMARY KEY("code")); ');

query.run();